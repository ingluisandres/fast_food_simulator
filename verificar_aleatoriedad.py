import math
import numpy as np


def prueba_de_media(numeros_aleatorios):
    """Este metodo comprueba si una lista de numeros son aleatorios."""
    n = len(numeros_aleatorios)

    media = round((sum(numeros_aleatorios)/n), 5)
    w = 1.96*(1/(12*math.sqrt(n)))
    limite_superior_media = round(0.5+w, 5)
    limite_inferior_media = round(0.5-w, 5)
    print("PRUEBA DE MEDIA")
    print(f"El limite inferior {limite_inferior_media}")
    print(f"El limite superior {limite_superior_media}")
    print(f"La media de los numeros aleatorios es {media}")

    if(limite_inferior_media <= media <= limite_superior_media):
        print("Se acepta la hipotesis H0 de parte de la prueba de media")
    print("\n")
    return


def prueba_de_frecuencia(numeros_aleatorios):
    """Este metodo comprueba si una lista de numeros son aleatorios.
        Se tiene que pasar una lista con numeros flotantes entre 0 y 1"""

    n = len(numeros_aleatorios)

    dicc_frecuencias = {
        "intervalo_a": [],
        "intervalo_b": [],
        "intervalo_c": [],
        "intervalo_d": [],
        "intervalo_e": [],
        "intervalo_f": [],
        "intervalo_g": [],
        "intervalo_h": [],
        "intervalo_i": [],
        "intervalo_j": [],
        "lista_de_intervalos": []}

    for i in range(n):
        if (0 < numeros_aleatorios[i] <= 0.1000):
            dicc_frecuencias["intervalo_a"].append(numeros_aleatorios[i])
        if (0.1000 < numeros_aleatorios[i] <= 0.2000):
            dicc_frecuencias["intervalo_b"].append(numeros_aleatorios[i])
        if (0.2000 < numeros_aleatorios[i] <= 0.3000):
            dicc_frecuencias["intervalo_c"].append(numeros_aleatorios[i])
        if (0.3000 < numeros_aleatorios[i] <= 0.4000):
            dicc_frecuencias["intervalo_d"].append(numeros_aleatorios[i])
        if (0.4000 < numeros_aleatorios[i] <= 0.5000):
            dicc_frecuencias["intervalo_e"].append(numeros_aleatorios[i])
        if (0.5000 < numeros_aleatorios[i] <= 0.6000):
            dicc_frecuencias["intervalo_f"].append(numeros_aleatorios[i])
        if (0.6000 < numeros_aleatorios[i] <= 0.7000):
            dicc_frecuencias["intervalo_g"].append(numeros_aleatorios[i])
        if (0.7000 < numeros_aleatorios[i] <= 0.8000):
            dicc_frecuencias["intervalo_h"].append(numeros_aleatorios[i])
        if (0.8000 < numeros_aleatorios[i] <= 0.9000):
            dicc_frecuencias["intervalo_i"].append(numeros_aleatorios[i])
        if (0.9000 < numeros_aleatorios[i] <= 1):
            dicc_frecuencias["intervalo_j"].append(numeros_aleatorios[i])

    dicc_frecuencias["lista_de_intervalos"] = [
        len(dicc_frecuencias["intervalo_a"]),
        len(dicc_frecuencias["intervalo_b"]),
        len(dicc_frecuencias["intervalo_c"]),
        len(dicc_frecuencias["intervalo_d"]),
        len(dicc_frecuencias["intervalo_e"]),
        len(dicc_frecuencias["intervalo_f"]),
        len(dicc_frecuencias["intervalo_g"]),
        len(dicc_frecuencias["intervalo_h"]),
        len(dicc_frecuencias["intervalo_i"]),
        len(dicc_frecuencias["intervalo_j"])]

    ve = 10
    suma_frecuencias = 0
    for i in range(len(dicc_frecuencias["lista_de_intervalos"])):
        suma_frecuencias += ((((dicc_frecuencias["lista_de_intervalos"])[i] - ve)**2)
                             / (dicc_frecuencias["lista_de_intervalos"])[i])
    print("PRUEBA DE FRECUENCIA")
    print(f"Revisar la tabla de chi cuadrada para encontrar el valor de " +
          "x²Tablas,  donde gl = celdas-1 y a = 0.5.")
    print(f"El x²Cal es {round(suma_frecuencias,5)}, comparalo con el " +
          "x²Tablas y si es menor probablemente los numeros sean aleatorios.")
    print("\n")
    return


def prueba_de_corridas(numeros_aleatorios):
    """Este metodo comprueba si una lista de numeros son aleatorios."""

    n = len(numeros_aleatorios)
    corrida = ""
    h = 0
    acumulacion = 1
    valor_observado = []

    for i in range(n-1):
        if(numeros_aleatorios[i] > numeros_aleatorios[i+1]):
            corrida += "1"
        else:
            corrida += "0"

    for i in range(len(corrida)-1):
        if(corrida[i] == corrida[i+1]):
            acumulacion += 1
        else:
            h += 1
            valor_observado.append(acumulacion)
            acumulacion = 1

    vo_corrida = {}
    for i in range(1, 13):
        vo_corrida[i] = valor_observado.count(i)

    def factorial(n):

        if (n != 0 and n != 1):
            return(n * factorial(n-1))
        else:
            return 1

    # 2. Paso for para calcular Valor esperado (VE)
    ve_corridas = {}
    for i in range(1, 13):
        temp1 = ((i**2)+(3*i)+1)*n
        temp2 = (i**3)+(3*(i**2))-i-4
        ve_corridas[i] = (2*((temp1)-(temp2))) / (factorial(i+3))
    # 3. Paso: for para calcular x² Cal
    suma_corridas = 0
    for i in range(1, 13):
        try:
            suma_corridas += (((vo_corrida[i] - ve_corridas[i])**2)
                              / ve_corridas[i])
        except ZeroDivisionError:
            pass
    # 4. Paso: impresion de resultados
    print("PRUEBA DE CORRIDAS")
    print(f"Revisar la tabla de chi cuadrada para encontrar el valor de " +
          "x²Tablas, donde gl = celdas-1 y a = 0.5.")
    print(f"El x²Cal es {round(suma_corridas,5)}, comparalo con el x²Tablas " +
          "y si es menor probablemente los numeros sean aleatorios.")
    print("\n")
    return

# Main ------------------------------------------------------------------------------


def main():
    x = np.random.uniform(0, 1, 100)
    print(x)
    print()
    prueba_de_media(x)
    prueba_de_corridas(x)
    prueba_de_frecuencia(x)

# Fin del Main ---------------------------------------------------------------------


if __name__ == "__main__":
    main()
