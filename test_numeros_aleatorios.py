import math
import unittest

import numeros_aleatorios as random

# Classes ---------------------------------------------------------------------------


class test_Aleatorios_random(unittest.TestCase):

    def test_valid_type(self):
        test_class = random.Aleatorios()
        test_result = test_class.random()
        expected_result = 0.416704
        self.assertEqual(test_result, expected_result)


class test_Aleatorios_uniform(unittest.TestCase):

    def test_valid_type(self):
        test_class = random.Aleatorios()
        test_result = test_class.uniform(3, 6)
        expected_result = 4.250112
        self.assertEqual(test_result, expected_result)


class test_Aleatorios_normal(unittest.TestCase):

    def test_valid_type(self):
        test_class = random.Aleatorios()
        test_result = test_class.normal(12, 4)
        expected_result = 10.106624
        self.assertEqual(test_result, expected_result)


class test_Aleatorios_exponential(unittest.TestCase):

    def test_valid_type(self):
        test_class = random.Aleatorios()
        test_result = test_class.exponential(5)
        expected_result = 4.376896
        self.assertEqual(test_result, expected_result)


# -----------------------------------------------------------------------------------

if(__name__ == '__main__'):
    unittest.main()
