import math


class Aleatorios():
    """Esta clase modela para generar variables aleatorias."""

    def __init__(self):
        self.modulo = 222647
        self.semilla1, self.semilla2, self.semilla3 = 9973, 4999, 7499

    def random(self):
        """Este metodo genera una lista de numeros aleatorios."""

        c = self.semilla1 + (self.semilla2*self.semilla3)
        r = c % self.modulo
        self.semilla1, self.semilla2, self.semilla3 = self.semilla2, self.semilla3, r
        r = r/self.modulo
        return round(r, 6)

    def uniform(self, lim_inf, lim_sup):
        """La distribucion de los numeros aleatorios sera uniforme"""

        r = self.random()
        x = lim_inf + (r*(lim_sup-lim_inf))
        return x

    def exponential(self, media):
        """La distribucion de los numeros aleatorios sera exponencial"""

        r = self.random()
        x = ((-math.log(r)) / media)*25
        return round(x, 6)

    def normal(self, media, des_est):
        """La distribucion de los numeros aleatorios sera normal"""

        lista = []
        for i in range(12):
            lista.append(self.random())
        suma = sum(lista) - 6
        x = media + (des_est * suma)
        return x

# Main  ---------------------------------------------------------------------------


def main():
    pass

# Fin del Main ---------------------------------------------------------------------


if __name__ == "__main__":
    main()
