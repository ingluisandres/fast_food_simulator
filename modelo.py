#!/usr/bin/env python
# -*- coding: utf-8 -*-


# ---------------------------------------------------------------------------

from numeros_aleatorios import Aleatorios
import time

# ---------------------------------------------------------------------------


__license__ = ""
__author__ = "Luis Andrés García Contreras"
__startdate__ = "20/07/2020"
__module__ = "modelo"
__python_version__ = "3.7.5"

__lastdate__ = ""
__version__ = ""


__all__ = ['']

# ---------------------------------------------------------------------------


class Modelo():
    """
    Una clase para representar una simulación de un restaurant.

    ...

    Componentes
    -----------    
    Cajero
    Clientes
    Cocineros

    Parametros
    ----------
    T_LLEGADA_MEDIA: 
        La media del tiempo llegada de los clientes.
    T_PLATILLO_MEDIA: 
        La media del tiempo de preparacion del platillo.
    T_PLATILLO_DES_EST: 
        La desviacion estandar del tiempo de preparacion del platillo.
    T_CAJA_LIM_SUP:
        El limite superior del tiempo en caja.
    T_CAJA_LIM_INF:
        El limite inferior del tiempo en caja.
    T_LIMITE:
        Tiempo total de la simulación.

    Variables de entrada (exogenas)
    --------------------
    t_llegada:
        Tiempo entre la llegada de los diferentes clientes.
    t_atencion:
        Tiempo que se tardan en atender al cliente.
    t_platillo:
        Tiempo que se tarda el cocinero en preparar el platillo.

    Variables de estado
    -------------------
    reloj:
        Contador de tiempo.
    fila_caja:
        Tamaño actual de la fila.
    fila_platillo:
        Tamaño actual de la fila.
    delta:
        Incremento
    n_clientes_que_llegaron:
        Numero total de clientes que han llegado.
    n_clientes_atendidos:
        Numero total de clientes atendidos.
    n_clientes_con_platillo:
        Numero total del clientes con platillo.
    num_emp:
        Cantidad de empleados dependiendo la hora.

    Variables de salida (endogenas)
    -------------------
    t_ocio_caja:
        Tiempo de ocio de caja.
    t_ocio_cocineros:
        Tiempo de ocio de todos los cocineros.
    t_ocio_total:
        Tiempo de ocio de caja y de los cocineros.
    t_e_promedio_caja:
        Tiempo de espera promedio de los clientes para caja.
    t_e_promedio_caja:
        Tiempo de espera promedio para el platillo.
    t_e_total_caja:
        Tiempo de espera total en la caja.
    t_e_total_platillo:
        Tiempo de espera total para el platillo.
    t_e_total:
        Tiempo de espera total de los clientes en ambas filas.
    """

    def __init__(self):
        """Se establecen los parametros"""

        self.T_LLEGADA__MEDIA = lee_entero(
            "Media del tiempo de llegada de los clientes: ")
        self.T_CAJA_LIM_INF = lee_entero("Minimo de tiempo de tomar orden: ")
        self.T_CAJA_LIM_SUP = lee_entero("Maximo de tiempo de tomar orden: ")
        self.T_PLATILLO__MEDIA = lee_entero("Media del tiempo del platillo: ")
        self.T_PLATILLO__DES_EST = lee_entero(
            "Desviacion estandar del tiempo del platillo: ")
        self.T_LIMITE = lee_entero("Tiempo limite de la simulacion: ")

    def __generar_llegada_del_cliente(self):
        """Este metodo asigna un valor aleatorio con distribucion exponencial"""

        temp = int(self.random.exponential(self.T_LLEGADA__MEDIA))
        return temp

    def __generar_atencion_al_cliente(self):
        """Este metodo asigna un valor aleatorio con distribucion uniforme"""

        temp = int(self.random.uniform(self.T_CAJA_LIM_INF,
                                       self.T_CAJA_LIM_SUP))
        return temp

    def __generar_entrega_de_platillo(self):
        """Este metodo asigna un valor aleatorio con distribucion normal"""
        temp = int(self.random.normal(self.T_PLATILLO__MEDIA,
                                      self.T_PLATILLO__DES_EST))
        return temp

    def condiciones_iniciales(self):
        """Este metodo establece las condiciones iniciales"""

        # Objeto aleatorio
        self.random = Aleatorios()

        # Reloj
        self.reloj__ = 0

        # Filas
        self.fila_caja__ = 0
        self.fila_platillo__ = 0

        # Delta
        self.delta__ = 1000

        # Tiempos de espera
        self.t_e_total_caja = 0
        self.t_e_promedio_caja = 0
        self.t_e_total_platillo = 0
        self.t_e_promedio_platillo = 0
        self.t_e_total = 0

        # Tiempos de ocio
        self.t_ocio_caja = 0
        self.t_ocio_cocineros = {1: 0, 2: 0, 3: 0, 4: 0}
        self.t_ocio_total = 0

        # Numero de clientes
        self.n_c_que_llegaron__ = 0
        self.n_c_atendidos__ = 0
        self.n_c_con_platillo__ = 0

        # Numero de cocineros por el tiempo
        self.n_cocineros__ = 0

        # Generadores de eventos
        self._t_llegada_ = self.__generar_llegada_del_cliente() + 1
        self._t_atencion_ = 0
        self._t_platillo_ = {1: 0, 2: 0, 3: 0, 4: 0}

    def __conseguir_delta(self):
        """Este metodo selecciona el proximo evento"""

        lista_eventos = [
            self._t_llegada_, self._t_atencion_, self._t_platillo_[1],
            self._t_platillo_[2], self._t_platillo_[3], self._t_platillo_[4]
        ]

        for i in range(lista_eventos.count(0)):
            lista_eventos.remove(0)

        self.delta__ = min(lista_eventos)

    def __actualizacion_del_sistema(self):
        """Este metodo actualiza el reloj, tiempo de espera total de caja, 
        tiempo de espera total para el platillo  y el tiempo de llegada 
        del proximo cliente"""

        self.reloj__ += self.delta__
        self.t_e_total_caja += (self.fila_caja__*self.delta__)
        self.t_e_total_platillo += (self.fila_platillo__*self.delta__)

    def __determinar_cocineros(self):
        """Este metodo determina los empleados segun el tiempo transcurrido
         en la simulacion"""

        if self.reloj__ < 240:
            self.n_cocineros__ = 2
        elif 240 < self.reloj__ < 480:
            self.n_cocineros__ = 3
        elif self.reloj__ > 480:
            self.n_cocineros__ = 4

    def __ajustar_tiempo_entre_llegadas(self):
        """Este metodo verifica si llego un nuevo cliente"""

        self._t_llegada_ = abs(self._t_llegada_-self.delta__)

        while(self._t_llegada_ == 0):
            self.n_c_que_llegaron__ += 1
            self.fila_caja__ += 1
            self._t_llegada_ = self.__generar_llegada_del_cliente()

    def __ajustar_tiempo_de_servicio(self):
        """Este metodo sirve para ver estado del cajero y para generar el 
        tiempo de atención."""

        self._t_atencion_ -= self.delta__

        if (self._t_atencion_ <= 0):

            if self._t_atencion_ < 0:
                self.t_ocio_caja -= self._t_atencion_
                self._t_atencion_ = 0
            elif self._t_atencion_ == 0:
                self.n_c_atendidos__ += 1
                self.fila_platillo__ += 1

            if self.fila_caja__ > 0:
                self.fila_caja__ -= 1
                self._t_atencion_ = self.__generar_atencion_al_cliente()
            else:
                pass

        else:
            pass

    def __ajustar_tiempo_del_platillo(self, cocinero):
        """Este metodo sirve para ver el estado de los cocineros y genera 
        el tiempo de entrega del platillo"""

        self._t_platillo_[cocinero] -= self.delta__

        if (self._t_platillo_[cocinero] <= 0):

            if self._t_platillo_[cocinero] < 0:
                self.t_ocio_cocineros[cocinero] -= self._t_platillo_[cocinero]
                self._t_platillo_[cocinero] = 0
            elif self._t_platillo_[cocinero] == 0:
                self.n_c_con_platillo__ += 1

            if self.fila_platillo__ > 0:
                self.fila_platillo__ -= 1
                self._t_platillo_[
                    cocinero] = self.__generar_entrega_de_platillo()
            else:
                pass

        else:
            pass

    def __imprimir_estado_del_sistema(self):
        """Este metodo imprime el estado actual del sistema"""

        # time.sleep(1)

        print("Reloj: " + str(self.reloj__) +
              ", Delta: " + str(self.delta__) +
              ", N_C_Lleg: " + str(self.n_c_que_llegaron__) +
              ", Fila (Caja): " + str(self.fila_caja__) +
              ", N_C_Aten: " + str(self.n_c_atendidos__) +
              ", Fila (Platillo): " + str(self.fila_platillo__) +
              ", N_C_Con_Platillo: " + str(self.n_c_con_platillo__))

    def verificar_tiempo_limite(self):
        """Este metodo verifica si ya se termino el tiempo establecido"""

        while(self.reloj__ < self.T_LIMITE):
            self.__conseguir_delta()
            self.__actualizacion_del_sistema()
            self.__determinar_cocineros()
            self.__ajustar_tiempo_entre_llegadas()
            self.__ajustar_tiempo_de_servicio()
            for i in range(1, self.n_cocineros__+1):
                self.__ajustar_tiempo_del_platillo(i)
            self.__imprimir_estado_del_sistema()

    def variables_endogenas(self):
        """Este metodo imprime las variables endogenas (De Salida)"""

        self.t_e_promedio_caja = self.t_e_total_caja/self.T_LIMITE
        self.t_e_promedio_platillo = self.t_e_total_platillo/self.T_LIMITE
        self.t_e_total = self.t_e_total_caja + self.t_e_total_platillo
        self.t_ocio = self.t_ocio_caja + sum(self.t_ocio_cocineros.values())

        print(f'''
        Variables Endogenas:
            Tiempo de espera promedio en caja: {round(self.t_e_promedio_caja,2)} minutos,
            Tiempo de ocio de caja: {self.t_ocio_caja} minutos,
            Tiempo de espera total en caja: {round(self.t_e_total_caja/60,2)} horas.

            Tiempo de espera promedio del platillo: {round(self.t_e_promedio_platillo,2)} minutos,
            Tiempo de ocio de los cocineros: {sum(self.t_ocio_cocineros.values())} minutos,
            Tiempo de espera total del platillo: {round(self.t_e_total_platillo/60,2)} horas.

            Tiempo de ocio total: {self.t_ocio} minutos,
            Tiempo de espera total de los clientes: {round(self.t_e_total/60,2)} horas.
        ''')

# ---------------------------------------------------------------------------


def lee_entero(mensaje):
    print(mensaje, end="")
    while True:
        entrada = input()
        try:
            entrada = int(entrada)
            return entrada
        except ValueError:
            print("Ingrese solamente enteros, porfavor: ", end="")

# Main  ---------------------------------------------------------------------------


def main():
    print("\nEsta es una simulación de un puesto de comida, " +
          "se te pide que ingreses los siguientes valores:")
    mi_modelo = Modelo()
    mi_modelo.condiciones_iniciales()
    print("\nHA EMPEZADO LA SIMULACION\n")
    mi_modelo.verificar_tiempo_limite()
    print("\nHA FINALIZADO LA SIMULACION\n")
    mi_modelo.variables_endogenas()

# Fin del Main ---------------------------------------------------------------------


if __name__ == "__main__":
    main()
